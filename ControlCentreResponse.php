<?php

namespace app\ControlCentreClient;

/**
 * A generated ControlCentreResponse object.
 * 
 * @requires PHP 7.0
 * @version 0.2
 * @see ControlCentreClient
 */
class ControlCentreResponse {
    private $requestObject = null;
    
    /**
     * @param ControlCentreClient $ccClientObject
     */
    public function __construct(ControlCentreClient $ccClientObject) {
        $this->requestObject = $ccClientObject;
    }
    
    /**
     * Did the Control Centre self-report that the query was successful?
     * @return boolean
     */
    public function isSuccessful() {
        $response = $this->getResponse();
        return !is_null($response) && $response['success'] === 1;
    }
    
    /**
     * Get the last response to the Control Centre.
     * Returns false if there was a failure getting the actual response.
     * This function would include all metadata that the Control Centre
     * will normally return. For a more precise response,
     * look at the @see entries.
     * @see $this::getData()
     * @see $this::getError()
     * @return object | null
     */
    public function getResponse() {
        $response = $this->requestObject->getResponse();
        
        // Typically, the response will only be null if the json_decode failed
        // which suggests that the response is malformed.
        if (empty($response)) {
            return null;
        } else {
            return $response;
        }
    }
    
    /**
     * The Control Centre returns all contextual data within the data key.
     * In the event that the data key is not in the response, then there is
     * probably an error.
     * @return array
     */
    public function getData() {
        $fullResponse = $this->getResponse();
        if ($this->isSuccessful() && !isset($fullResponse['results']['data'])) {
            return $fullResponse['results'];
        }
        return $fullResponse['results']['data'];
    }
    
    /**
     * 
     * @return array
     */
    public function getError() {
        $fullResponse = $this->getResponse();
        return $fullResponse['error'];
    }
}
