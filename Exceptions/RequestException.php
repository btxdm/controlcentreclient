<?php

namespace app\ControlCentreClient\Exceptions;

use Exception;

class RequestException extends Exception {
    private $rawResponse;
    private $payload;
    
    /**
     * @param string $message
     * @param mixed $rawResponse
     * @param mixed $payload
     */
    public function __construct(string $message, $rawResponse = [], $payload = [], string $url = '') {
        $this->rawResponse = $rawResponse;
        $this->payload = $this->removeSensitiveDataFromPayload($payload);
        $this->url = $url;
        parent::__construct($message);
    }
    
    /**
     * Extends \Exception getMessage to prepend the server name.
     * This is useful when constructing an exception email subject line.
     * @return string
     */
    public function getFullMessage() : string {
        $exceptionMessage = parent::getMessage();
        return "{$_SERVER['SERVER_NAME']} Exception calling '{$this->getCalledFunction()}': $exceptionMessage";
    }
    
    /**
     * Get the full payload sent to the API.
     * @return array
     */
    public function getPayload() : array {
        return $this->payload;
    }
    
    /**
     * Get the payload, excluding auth (containing username and password) to the API.
     * @return array
     */
    public function removeSensitiveDataFromPayload($payload) : array {
        unset($payload['auth']);
        return $payload;
    }
    
    /**
     * Get the response of the API request before it was decoded.
     * @return mixed
     */
    public function getRawResponse() {
        return $this->rawResponse;
    }
    
    /**
     * Get the function that the client attempted to call.
     * @return string
     */
    public function getCalledFunction() : string {
        return $this->payload['func'];
    }
}
