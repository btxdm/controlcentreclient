<?php

namespace app\ControlCentreClient;

use \Exception as Exception;
use \app\ControlCentreClient\Exceptions\RequestException as RequestException;
use \app\ControlCentreClient\ControlCentreResponse as ControlCentreResponse;

/**
 * Connects to the Control Centre API.
 * 
 * @requires PHP 7.0
 * @version 0.2
 */
class ControlCentreClient {
    
    private $username;
    private $password;    
    private $mode;
    private $url;
    protected $userAgent = NULL;
    protected $clientAddress = NULL;
    private $response;
    private $namespace;
    private $loginAsCust;
    
    const MODE_TEST = 'test';
    const MODE_LIVE = 'live';
    const DEFAULT_VERSION = '0.1';

    /**
     * 
     * @param string $username
     * @param string $password
     * @param string $namespace
     * @param string $mode
     * @param string $version
     * @return $this
     * @throws Exception
     */
    public function __construct(
            string $username,
            string $password,
            string $namespace = '',
            string $mode = self::MODE_LIVE,
            $version = self::DEFAULT_VERSION
        ) {
        $this->version = $version;
        $this->username = $username;
        $this->password = $password;
        $this->namespace = $namespace;
        
        // Does the mode match the internal type?
        if (!in_array($mode, [
            self::MODE_TEST,
            self::MODE_LIVE,
        ])) {
            throw new Exception('Mode must be one of the valid MODE types defined as a constant paramater of ' . __CLASS__);
        }
        $this->mode = $mode;
        
        $this->url = "https://{$this->mode}.apicontrol.co.uk/api/{$this->version}/json";
        
        $className = __CLASS__;
        $this->userAgent = "php/{$className}/{$this->version}";

        if (isset($_SERVER['REMOTE_ADDR'])) {
            $this->clientAddress = $_SERVER['REMOTE_ADDR'];
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $this->clientAddress .= ', ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
    }
    
    /**
     * Wrapper for curlCall or the method of choice.
     * @param string $methodName
     * @param array $arguments
     * @return ControlCentreResponse
     */
    public function call(string $methodName, $arguments = null) : ControlCentreResponse {
        return $this->curlCall($methodName, $arguments);
    }
    
    /**
     * Used when calling the ControlCentreClient with the Control Centre
     * function name as a method on the object.
     * @param string $name Function name
     * @param array $arguments Array of function arguments
     * @return ControlCentreResponse
     * @throws Exception
     */
    public function __call(string $name, $arguments = null) : ControlCentreResponse {
        if (isset($arguments)) {
            if (count($arguments) > 1) {
                throw new Exception("Too many arguments passed to $name", 2);
            }
            $arguments = array_shift($arguments);
        }
        
        return $this->call(str_replace('_', '.', $name), $arguments);
    }
    
    /**
     * Overloadable method to get the response using cURL. All cURL behaviour should be kept in here.
     * If another method is available, overload the call method.
     * @param string $methodName
     * @param array $arguments
     * @throws Exceptions\RequestException
     * @return ControlCentreResponse
     */
    protected function curlCall(string $methodName, $arguments = null, $exceptions = true) : ControlCentreResponse {
        // Store the Curl Instance for future use in the current process lifetime.
        $instance = curl_init();

        $authParams = [
            'username' => $this->username,
            'password' => $this->password
        ];

        if (!empty($this->namespace)) {
            $authParams['namespace'] = $this->namespace;
        }
        
        if (empty($arguments)) {
            $arguments = [];
        }
        
        curl_setopt($instance, CURLOPT_POST, 1);

        if (!empty($this->loginAsCust)) {
            $authParams['login_as']['custID'] = $this->loginAsCust;
        }
        
        $payload = [
            'auth' => $authParams,
            'func' => $methodName,
            'args' => $arguments
        ];
        
        curl_setopt($instance, CURLOPT_POSTFIELDS, json_encode($payload));

        curl_setopt($instance, CURLOPT_URL, $this->url);
        curl_setopt($instance, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($instance, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($instance, CURLOPT_SSL_VERIFYPEER, TRUE);
        curl_setopt($instance, CURLOPT_USERAGENT, $this->userAgent);
        if ($this->clientAddress !== NULL) {
            curl_setopt($instance, CURLOPT_HTTPHEADER, array('X-Forwarded-For: ' . $this->clientAddress));
        }
        curl_setopt($instance, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        $rawResponse = curl_exec($instance);
        $response = $this->response = json_decode($rawResponse, true);

        $error = curl_error($instance);
        
        include_once 'ControlCentreResponse.php';
        include_once 'Exceptions/RequestException.php';
        
        $exceptionArguments = [
            $rawResponse, $payload, $this->url
        ];
        
        // Can the page be caught at all?
        $curlResponseCode = curl_getinfo($instance, CURLINFO_HTTP_CODE);
        if ($curlResponseCode !== 200) {
            throw new RequestException("Got {$curlResponseCode} when trying to CURL to the URL.", ...$exceptionArguments);
        }
        
        // JSON cannot be decoded.
        if ($response === null) {
            throw new RequestException("JSON could not be decoded.", ...$exceptionArguments);
        }
        
        // The function can choose to ignore some exceptions.
        // This is useful if you genuinely want to handle 
        // malformed data, or error states.
        if ($exceptions) {

            if (isset($response['error'])) {
                throw new RequestException("Control Centre responded with error: {$response['error']}.", ...$exceptionArguments);
            }

            // Set if the Control Centre self-reports an error with the request.
            if (isset($response['success']) && $response['success'] === 0) {
                throw new RequestException($response, ...$exceptionArguments);
            }
        }
        
        curl_close($instance);
        
        return new ControlCentreResponse($this);
    }
    
    /**
     * @return mixed Contains the decoded response.
     */
    public function getResponse() {
        return $this->response;
    }
    
    /**
     * Make this request as an admin user but with the customer data
     * of the supplied customer ID.
     * @param int $custID
     * @return object Returns self for chaining.
     */
    public function setLoginAs(int $custID) {
        $this->loginAsCust = $custID;
        return $this;
    }
}

